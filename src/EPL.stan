data {
  int<lower=1> nteams; 
  int<lower=1> ngames; 
  int<lower=1, upper=nteams> home_team[ngames];
  int<lower=1, upper=nteams> away_team[ngames];
  vector[ngames] score_diff_sqrt;
  vector<lower=0, upper=1>[nteams] prev_perf; // Last season performance
  int<lower=1> df;
}
parameters {
  real b;
  real<lower=0> sigma_a;
  real<lower=0> sigma_y;
  vector[nteams] eta_a;
}
transformed parameters {
  vector[nteams] a;
  a = b*prev_perf + sigma_a*eta_a;
}
model {
  b ~ normal(0,1);
  eta_a ~ normal(0,1);
  score_diff_sqrt ~ student_t(df, a[home_team] - a[away_team], sigma_y);
}

