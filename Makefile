## First, extract parameters from the text-based makefile into an R
## list saved into an RDS object file. This just makes it easier to access
## parameters by name during model execution and post-processing
output/data_file.Rds: munge/extract_data.R  data/parameters.csv data/prev_perf.csv
	@echo --- Extracting parameters and downloading and saving the data ---
	@mkdir -p $(@D)
	./$< -o $@ -p $(word 2, $^) -m $(word 3, $^)

output/stan_samples.rds: src/fit_model.R output/data_file.rds src/EPL.stan 
	@echo --- Fitting the model using Stan ---
	@mkdir -p $(@D)
	./$< -o $@ -p $(word 2, $^) -m $(word 3, $^) -c 4 -i 1000
	
	